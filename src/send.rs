#[derive(Debug)]
pub enum SendError {
    ResultError,
    ParseFailed,
    ReadFailed,
    WriteFailed,
}
pub trait Send {
    fn send_command(&mut self, command: &str) -> Result<(), SendError>;
}
