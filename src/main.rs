use send::SendError;
use send::Send;
use clap::Clap;
use log::{debug, error, info, warn};
use serialport::prelude::*;
use serialport::SerialPortSettings;
use std::time::Duration;

mod send;

#[derive(Clap)]
// #[clap(version = crate_version!(), author = crate_authors!())]
struct Opts {
    ///Number to send to
    number: String,
    ///SMS message contents
    message: String,
}

fn main() {
    env_logger::init();
    let opts = Opts::parse();
    let s = SerialPortSettings {
        baud_rate: 115200,
        data_bits: DataBits::Eight,
        flow_control: FlowControl::None,
        parity: Parity::None,
        stop_bits: StopBits::One,
        timeout: Duration::from_millis(30000),
    };
    let mut result =
        serialport::open_with_settings("/dev/ttyS0", &s).expect("Failed to open serial port");
    result.send_command("AT").expect("Can't reach modem");
    result
        .send_command("AT+CMGF=1")
        .expect("Modem doesn't provide Text mode");
    result
        .send_command(&format!("AT+CMGS=\"{}\"", opts.number))
        .expect("Failed to set phone number");
    result
        .send_command(&format!("{}\x1A", opts.message))
        .expect("Faile to send text message");
}


impl Send for dyn SerialPort {
    fn send_command(&mut self, command: &str) -> Result<(), SendError> {
        let command = format!("{}\r", command);
        match self.write(command.as_bytes()) {
            Ok(bytes_written) => {
                debug!("Send command ({} bytes): {}", bytes_written, command);

                let mut output = [0; 512];
                match self.read(&mut output) {
                    Ok(bytes_read) => {
                        debug!(
                            "Command response ({} bytes): {:?}",
                            bytes_read,
                            &output[0..bytes_read]
                        );
                        match std::str::from_utf8(&output[0..=bytes_read]) {
                            Ok(x)
                                if x == "\r\nOK\r\n\x00"
                                    || x == "\r\n+CMGS:\x00"
                                    || x == "\r\n> \x00" =>
                            {
                                info!("Command ({:?}) successfully executed", command)
                            }
                            Ok("\r\nERROR\r\x00") => {
                                error!("Command failed: {:?}:", command);
                                return Err(SendError::ResultError);
                            }
                            Ok(response) => {
                                warn!("Command completed with unknown result: {:?}", response);
                                return Err(SendError::ParseFailed);
                            }
                            Err(error) => {
                                error!("Command completed with unparsable result: {}", error);
                                return Err(SendError::ParseFailed);
                            }
                        };
                    }
                    Err(error) => {
                        error!("Error reading command response: {}", error);
                        return Err(SendError::ReadFailed);
                    }
                }
            }
            Err(error) => {
                error!("Error sending command: {}", error);
                return Err(SendError::WriteFailed);
            }
        }

        Ok(())
    }
}
