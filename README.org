#+TITLE: Readme
#+OPTIONS: toc:nil \n:t ':nil ^:nil

* Aprašymas
SMS siuntimo CLI programa.
Skirta komunikuoti AT komandomis su modem'u

* Reikalavimai
- rustc kompiliatorius
- cargo
  
* Kompiliavimas
- Atsidaromas katalogas per CMD/PS/Terminal
- paleidžiama "cargo build" komanda
  
* Instaliavimas
- Nukopijuojamas executable failas
  
* Testavimas
Nėra
